# Description
-------------------------------------------------------------
This software is intended to be used by the system administrators of an office, and may not be used to do any harm, all the opposite, this software is intended to protect the running machine from **non-authorized** USB devices, such as *[rubber duckies](https://interferencias.tech/2017/11/07/rubber-ducky/)* or infected USB drives that may harm the system.
